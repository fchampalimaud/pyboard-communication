Release Notes
=============


Upgrading
---------
TODO

Maintenance team
----------------

The current and past members of the pyControl team.

* `@cajomferro <https://github.com/cajomferro/>`_ Carlos Mão de Ferro
* `@UmSenhorQualquer <https://github.com/UmSenhorQualquer/>`_ Ricardo Ribeiro
* `@takam <https://bitbucket.org/takam/>`_ Thomas Akam

Changes log
-----------

TODO