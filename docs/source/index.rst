.. pyboard_communication documentation master file, created by
   sphinx-quickstart on Thu Dec 15 12:00:20 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyboard-communication's documentation!
=========================================
This project is a library that abstracts `pyBoard <http://docs.micropython.org/en/latest/pyboard/pyboard/quickref.html>`_ communication details when used with the pyControl framework.

If you are looking for pyControl generic information please refer to http://pycontrol.readthedocs.io .


Contents:

.. toctree::
   :maxdepth: 2

   library_reference/index
   about/index





Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
