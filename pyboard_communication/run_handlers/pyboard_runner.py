# !/usr/bin/python
# -*- coding: utf-8 -*-

import logging

from multiprocessing import Event

from pyboard_communication.comm.pycontrol import PyControl
from pyboard_communication.run_handlers import PybranchRunHandler

logger = logging.getLogger(__name__)


class PyBoardRunner(PyControl, PybranchRunHandler):
	"""

	"""

	def __init__(self,
	             serial_port=None, baudrate=115200, inter_char_timeout=1,
	             in_queue=None, out_queue=None, refresh_time=None):
		"""

		:param in_queue:
		:param out_queue:
		:param refresh_time:
		"""

		# Event flag used to cancel the send_command loop
		self._send_command_evt_flag = Event()

		PybranchRunHandler.__init__(self, in_queue, out_queue, refresh_time)
		PyControl.__init__(self, serial_port, baudrate, inter_char_timeout)
