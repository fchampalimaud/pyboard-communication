#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os

SETTINGS_PRIORITY = 120

# THESE SETTINGS ARE NEEDED FOR PYINSTALLER
USE_MULTIPROCESSING = True

PYBOARD_COMMUNICATION_THREAD_REFRESH_TIME  = None # timer for thread look for events (seconds)
PYBOARD_COMMUNICATION_PROCESS_REFRESH_TIME = None # timer for process look for events (seconds)

PYBOARD_COMMUNICATION_PROCESS_TIME_2_LIVE = None # wait before killing process (seconds)


PYBOARD_COMMUNICATION_UPLOAD_CHUNCK = 512