# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pybranch.comm_error import CommError


class PyboardCommError(CommError):
	"""
	Exception class for errors related to pyboard communication
	"""

	def __init__(self, arg):
		super(PyboardCommError, self).__init__("\n\n---- PYBOARD RESPONSE ----\n" + arg)
