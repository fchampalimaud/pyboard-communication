# !/usr/bin/python
# -*- coding: utf-8 -*-

import logging

from pybranch.thread_handlers.qt_thread import QtThread

from pyboard_communication.async.async_pyboard import AsyncPyboard

logger = logging.getLogger(__name__)


class QtAsyncPyboard(AsyncPyboard):
	"""
	Qt implementation for AsyncPyboard
	"""

	def create_async_thread(self):
		qtt = QtThread(out_queue=self.out_queue,
		               in_queue=self.in_queue,
		               wait_for_results_fn=self.wait_for_results,
		               event_executor_fn=self.event_executor, mainwindow=self.mainwindow)

		logger.debug("Created QtThread")

		return qtt
