# !/usr/bin/python
# -*- coding: utf-8 -*-

import logging

from pyboard_communication.run_handlers.pyboard_runner import PyBoardRunner
from pybranch.thread_handlers.async_handler import AsyncHandler
from pybranch.thread_handlers.async_handler import dummy
from pybranch.thread_handlers.simple_thread import SimpleThread

logger = logging.getLogger(__name__)


class AsyncPyboard(AsyncHandler):
	"""
	Provides info and actions related to serial connection when using multiprocessing
	"""

	def __init__(self, serial_port=None, baudrate=115200, inter_char_timeout=1,
	             thread_refresh_time=None, runner_refresh_time=None):
		"""

		:param serial_port:
		:param baudrate:
		:param inter_char_timeout:
		:param thread_refresh_time:
		"""

		self.serial_port = serial_port
		self.baudrate = baudrate
		self.inter_char_timeout = inter_char_timeout

		AsyncHandler.__init__(self, thread_refresh_time=thread_refresh_time, runner_refresh_time=runner_refresh_time)

	#######################################################################################
	###### OVERRIDE METHODS ###############################################################
	#######################################################################################


	def create_runner(self):
		_runner = PyBoardRunner(
			serial_port=self.serial_port,
			baudrate=self.baudrate,
			inter_char_timeout=self.inter_char_timeout,
			in_queue=self.in_queue,
			out_queue=self.out_queue,
			refresh_time=self.runner_refresh_time)

		logger.debug("Created PyBoardRunner")

		return _runner  # type: PyBoardRunner

	def create_async_thread(self):
		st = SimpleThread(out_queue=self.out_queue,
		                  in_queue=self.in_queue,
		                  wait_for_results_fn=self.wait_for_results,
		                  event_executor_fn=self.event_executor)

		logger.debug("Created SimpleThread")

		return st

	def start_handler_execution(self):
		AsyncHandler.start_handler_execution(self)

	def stop_handler_execution(self):
		AsyncHandler.stop_handler_execution(self)
		if self.runner and self.runner.is_running:
			self.runner.close_serial_port()
		logger.debug("Execution handler stopped")

	#######################################################################################
	###### FUNCTIONS ######################################################################
	#######################################################################################

	def send_command(self, command, handler_evt=dummy, extra_args=None, group=None):
		"""

		:param command:
		:param handler_evt:
		:param extra_args:
		:param group:
		:return:
		"""
		self.call_function('send_command', args=(command,), handler_evt=handler_evt, extra_args=extra_args, group=group)
		return self.runner._send_command_evt_flag

	def exec_command(self, command, sleep_time=None, eval_output=True, handler_evt=dummy, extra_args=None, group=None):
		"""

		:param command:
		:param sleep_time:
		:param eval_output:
		:param handler_evt:
		:param extra_args:
		:param group:
		"""
		self.call_function('exec_command', args=(command, sleep_time, eval_output), handler_evt=handler_evt,
		                   extra_args=extra_args, group=group)

	def write(self, data, handler_evt=dummy, extra_args=None, group=None):
		"""

		:param data:
		:param handler_evt:
		:param extra_args:
		:param group:
		"""
		self.call_function('write', args=(data,), handler_evt=handler_evt, extra_args=extra_args, group=group)

	def reset(self, handler_evt=dummy, extra_args=None, group=None):
		"""

		:param handler_evt:
		:param extra_args:
		:param group:
		"""
		self.call_function('reset', handler_evt=handler_evt, extra_args=extra_args, group=group)

	def hard_reset(self, handler_evt=dummy, extra_args=None, group=None):
		"""

		:param handler_evt:
		:param extra_args:
		:param group:
		"""
		self.call_function('hard_reset', handler_evt=handler_evt, extra_args=extra_args, group=group)

	def reset_filesystem(self, handler_evt=dummy, extra_args=None, group=None):
		"""

		:param handler_evt:
		:param extra_args:
		:param group:
		"""
		self.call_function('reset_filesystem', handler_evt=handler_evt, extra_args=extra_args, group=group)

	def unique_id(self, handler_evt=dummy, extra_args=None, group=None):
		"""

		:param handler_evt:
		:param extra_args:
		:param group:
		"""
		self.call_function('unique_id', handler_evt=handler_evt, extra_args=extra_args, group=group)

	def isdir(self, path, handler_evt=dummy, extra_args=None, group=None):
		"""

		:param path:
		:param handler_evt:
		:param extra_args:
		:param group:
		"""
		self.call_function('isdir', args=(path,), handler_evt=handler_evt, extra_args=extra_args, group=group)

	def listdir(self, path, handler_evt=dummy, extra_args=None, group=None):
		"""

		:param path:
		:param handler_evt:
		:param extra_args:
		:param group:
		"""
		self.call_function('listdir', args=(path,), handler_evt=handler_evt, extra_args=extra_args, group=group)

	def mkdir(self, path, handler_evt=dummy, extra_args=None, group=None):
		"""

		:param path:
		:param handler_evt:
		:param extra_args:
		:param group:
		"""
		self.call_function('mkdir', args=(path,), handler_evt=handler_evt, extra_args=extra_args, group=group)

	def rmdir(self, path, handler_evt=dummy, extra_args=None, group=None):
		"""

		:param path:
		:param handler_evt:
		:param extra_args:
		:param group:
		"""
		self.call_function('rmdir', args=(path,), handler_evt=handler_evt, extra_args=extra_args, group=group)

	def rmfile(self, path, handler_evt=dummy, extra_args=None, group=None):
		"""

		:param path:
		:param handler_evt:
		:param extra_args:
		:param group:
		"""
		self.call_function('rmfile', args=(path,), handler_evt=handler_evt, extra_args=extra_args, group=group)

	def rename(self, old_path, new_path, handler_evt=dummy, extra_args=None, group=None):
		"""

		:param old_path:
		:param new_path:
		:param handler_evt:
		:param extra_args:
		:param group:
		"""
		self.call_function('rename', args=(old_path, new_path), handler_evt=handler_evt, extra_args=extra_args,
		                   group=group)

	def stat(self, path, handler_evt=dummy, extra_args=None, group=None):
		self.call_function('stat', args=(path,), handler_evt=handler_evt, extra_args=extra_args, group=group)

	def gc_collect(self, handler_evt=dummy, extra_args=None, group=None):
		self.call_function('gc_collect', handler_evt=handler_evt, extra_args=extra_args, group=group)

	def upload_file(self, local_path, remote_path, handler_evt=dummy, extra_args=None, group=None):
		self.call_function('upload_file', args=(local_path, remote_path), handler_evt=handler_evt,
		                   extra_args=extra_args, group=group)

	def upload_dir(self, local_path, remote_path, handler_evt=dummy, extra_args=None, group=None):
		self.call_function('upload_dir', args=(local_path, remote_path), handler_evt=handler_evt, extra_args=extra_args,
		                   group=group)

	def download_file(self, remote_path, local_path, handler_evt=dummy, extra_args=None, group=None):
		self.call_function('download_file', args=(remote_path, local_path), handler_evt=handler_evt,
		                   extra_args=extra_args, group=group)

	def download_dir(self, remote_path, local_path, handler_evt=dummy, extra_args=None, group=None):
		self.call_function('download_dir', args=(remote_path, local_path), handler_evt=handler_evt,
		                   extra_args=extra_args, group=group)

	def pycontrol_sync_variables(self, task_name, variables, handler_evt=dummy, extra_args=None, group=None):
		self.call_function('pycontrol_sync_variables', args=(task_name, variables), handler_evt=handler_evt,
		                   extra_args=extra_args, group=group)

	def pycontrol_install_framework(self, framework_path, devices_path, hardware_path, handler_evt=dummy,
	                                extra_args=None, group=None):
		self.call_function('pycontrol_install_framework', args=(framework_path, devices_path, hardware_path),
		                   handler_evt=handler_evt,
		                   extra_args=extra_args, group=group)

	def pycontrol_upload_task(self, task_path, handler_evt=dummy, extra_args=None, group=None):
		self.call_function('pycontrol_upload_task', args=(task_path,), handler_evt=handler_evt,
		                   extra_args=extra_args, group=group)
