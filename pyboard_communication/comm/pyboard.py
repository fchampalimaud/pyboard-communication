# !/usr/bin/python
# -*- coding: utf-8 -*-

import os
import logging
from time import sleep
from stat import S_ISDIR
import datetime
from pysettings import conf

from pyboard_communication.comm.base_serial import BaseSerial
from pyboard_communication.pyboard_comm_error import PyboardCommError

logger = logging.getLogger(__name__)


class Pyboard(BaseSerial):
	def __init__(self, serial_port, baudrate=115200, inter_char_timeout=1):
		"""

		:param serial_port:
		:param baudrate:
		:param inter_char_timeout:
		"""

		BaseSerial.__init__(self, serial_port, baudrate, inter_char_timeout)

	#######################################################################################
	###### FUNCTIONS ######################################################################
	#######################################################################################

	def open_serial_port(self):
		"""
		Overrides SerialProcess open
		"""
		if not self.connection.is_open:
			self.connection.open()
			self.enter_raw_repl()

	def close_serial_port(self):
		"""
		Closes serial connection now
		"""
		if self.connection:
			self.connection.close()
		else:
			logger.warning("Trying to close connection that is None")

	def enter_raw_repl(self):
		self.connection.write(b'\r\x03\x03')  # ctrl-C twice: interrupt any running program
		self.connection.write(b'\r\x01')  # ctrl-A: enter raw REPL

		command_res = ''
		while True:
			res = self.connection.read()
			if res == b'\x04': continue
			if res: command_res += res.decode("utf8")
			if command_res[-10:] == 'to exit\r\n>': break
			self.connection.flush()

	def send_command(self, command):
		"""
		Send command to process to be executed on board
		:param command:
		"""
		self.open_serial_port()

		self.connection.flush()
		self.connection.reset_input_buffer()
		self.connection.reset_output_buffer()

		self.connection.write(bytes(command, encoding='utf8'))
		self.connection.write(b'\x04')
		self.connection.flush()
		if self.read_until_ends_width(b'OK'):
			while not self._send_command_evt_flag.is_set():
				if self.refresh_time: sleep(self.refresh_time)
				line = self.readline()
				if line: self.log_msg(line, False, self._current_evt_idx)
		else:
			if self.refresh_time: sleep(self.refresh_time)
			data = self.connection.read(self.connection.in_waiting).decode("utf8")
			raise Exception(
				'===== Remote exception =====\n\n' + data + "=========================="
			)
		self._send_command_evt_flag.clear()

		return None

	def exec_command(self, command, sleep_time=None, eval_output=True):
		self.open_serial_port()

		self.connection.flush()
		self.connection.reset_input_buffer()
		self.connection.reset_output_buffer()

		self.connection.write(bytes(command, encoding='utf8'))
		self.connection.write(b'\r\n\x04')
		self.connection.flush()

		# if self.read_until_ends_width(b'OK', max_n_bytes=3):
		if self.read_until_ends_width(b'OK'):
			self.connection.flush()

			data = b''
			# wait for the data to be available
			# if sleep_time is not None: sleep(sleep_time)
			while (data[-2:] != b'\x04>'):
				data += self.connection.read(self.connection.in_waiting)

			if data[-3:] != b'\x04\x04>':
				data = data[:-2]
				raise PyboardCommError(data.decode("utf8"))
			else:
				data = data[:-3]

			return data[:-2].decode("utf8")

	def write(self, data):
		return self.connection.write(data)

	def isdir(self, path):
		stats = self.stat(path)
		if stats:
			return S_ISDIR(stats[0])
		else:
			return False

	def reset(self):
		logger.debug("Resetting board")
		self.write(b'\x04')  # ctrl-D: soft reset
		command_res = ''
		self.read_until_ends_width(b'to exit\r\n>')
		return None

	def hard_reset(self):
		self.connection.write(bytes('import pyb; pyb.hard_reset()', encoding='utf8'))
		self.connection.write(b'\x04')
		self.close()
		sleep(0.1)

	def reset_filesystem(self):
		logger.debug("Resetting filesystem...")
		for node in self.listdir('.'):
			if node not in ['System Volume Information', 'boot.py']:
				if self.isdir(node):
					self.rmdir(node)
				else:
					self.rmfile(node)
		# self.hard_reset()
		self.reset()

	def readline(self):
		buf = ''
		c = None
		self.connection.timeout = 1
		last_read = datetime.datetime.now()

		while c != b'\n':
			if self.connection.in_waiting:
				c = self.connection.read()
				if c != b'\n': buf += c.decode('utf8')
				if c is None: break
				last_read = datetime.datetime.now()
			if ((datetime.datetime.now() - last_read).total_seconds() * 1000.0) > 200:
				break

		return None if len(buf) == 0 else buf

	def read_until_ends_width(self, data, max_n_bytes=100000000000):
		output = b''
		while True:
			if self.connection.in_waiting > 0:
				c = self.connection.read()
				if c == b'\x04': continue
				if c:
					output += c
					if output[-len(data):] == data: return True
				if len(output) >= max_n_bytes:
					return False

	def unique_id(self):
		return self.exec_command('import pyb; print(pyb.unique_id())')

	def listdir(self, path):
		return eval(self.exec_command('import os; print( os.listdir("{0}") );'.format(path), 0.1))

	def mkdir(self, path):
		return self.exec_command('import os; os.mkdir("{0}")'.format(path))

	def rmdir(self, path):
		files = self.listdir(path)
		if len(files) > 0:
			for f in files:
				file_path = '/'.join([path, f])
				if self.isdir(file_path):
					self.rmdir(file_path)
				else:
					self.rmfile(file_path)

		return self.exec_command('import os; os.rmdir("{0}")'.format(path))

	def rmfile(self, path):
		return self.exec_command('import os; os.remove("{0}")'.format(path))

	def rename(self, old_path, new_path):
		return self.exec_command('import os; os.rename("{0}", "{1}")'.format(old_path, new_path))

	def stat(self, path):
		try:
			return eval(self.exec_command('import os; print( os.stat("{0}") )'.format(path), 0.01))
		except:
			return None

	def gc_collect(self):
		return self.exec_command('import gc; gc.collect()')

	def upload_file(self, local_path, remote_path):
		'''Copy a file into the root directory of the pyboard.'''

		logger.debug("Uploading file: %s", local_path)

		filename = os.path.split(local_path)[-1]

		if not filename.startswith("."):

			command = 'import os; os.sync()'
			self.exec_command(command)
			if self.isdir(remote_path) or remote_path == '.':
				remote_file_path = '/'.join([remote_path, filename])
			else:
				remote_file_path = remote_path

			command = 'instream = open("{0}", "w")'.format(remote_file_path)
			self.exec_command(command)

			with open(local_path, 'r') as in_streem:
				content = in_streem.read()

				n_bytes = conf.PYBOARD_COMMUNICATION_UPLOAD_CHUNCK
				for i in range(0, len(content) + n_bytes, n_bytes):

					if i >= len(content): break

					end = (i + n_bytes) if len(content[i:]) >= n_bytes else (i + len(content[i:]))
					command = 'instream.write({})'.format(repr(content[i:end]))
					self.exec_command(command)

			command = 'instream.close()'
			self.exec_command(command)

			self.gc_collect()
		else:
			logger.debug("File skip: %s", filename)

	def upload_dir(self, local_path, remote_path):
		logger.debug("Uploading dir: %s", local_path)
		dir_name = os.path.split(local_path)[-1]
		remote_path = '/'.join([remote_path, dir_name])

		if not self.isdir(remote_path): self.mkdir(remote_path)

		for node_name in os.listdir(local_path):
			node_path = '/'.join([local_path, node_name])

			if os.path.isdir(node_path):
				remote_dir = '/'.join([remote_path, node_name])
				if not self.isdir(remote_dir): self.mkdir(remote_dir)
				self.upload_dir(node_path, remote_dir)
			else:
				self.upload_file(node_path, remote_path)

	def download_file(self, remote_path, local_path):
		filename = '/'.split(remote_path)[-1]

		command = 'outstream = open("{0}", "r")'.format(remote_path)
		self.exec_command(command)

		command = 'for line in outstream: print(line, end="")'
		data = self.exec_command(command, sleep_time=1.0, eval_output=False)

		command = 'outstream.close()'
		self.exec_command(command)

		output_path = os.path.join(local_path, filename)
		outfile = open(output_path, 'wb')
		outfile.write(data)
		outfile.close()

	def download_dir(self, remote_path, local_path):
		for node_name in self.listdir(remote_path):
			node_path = '/'.join([remote_path, node_name])

			if self.isdir(node_path):
				local_dir = os.path.join(local_path, node_name)
				if not os.path.exists(local_dir): os.makedirs(local_dir)
				self.download_dir(node_path, local_dir)
			else:
				self.download_file(node_path, local_path)
