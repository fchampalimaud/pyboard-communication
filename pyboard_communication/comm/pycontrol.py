# !/usr/bin/python
# -*- coding: utf-8 -*-


from pyboard_communication.comm.pyboard import Pyboard


class PyControl(Pyboard):
	"""

	"""

	def pycontrol_install_framework(self, framework_path, devices_path, hardware_path):
		"""

		:param str framework_path:
		:param str devices_path:
		:param str hardware_path:
		"""
		self.open_serial_port()
		self.reset_filesystem()
		self.upload_dir(framework_path, '.')
		self.upload_dir(devices_path, '.')
		if hardware_path:
			self.upload_file(hardware_path, 'hardware_definition.py')
		self.reset()

	def pycontrol_upload_task(self, task_path):
		"""
		Install task by uploading file and reset after
		:param str task_path: task file to be uploaded
		"""
		self.open_serial_port()
		self.upload_file(task_path, '.')
		self.reset()

	def pycontrol_sync_variables(self, task_name, variables, queue=None):
		"""

		:param str task_name:
		:param ??? variables:
		:param ??? queue:
		:return:
		"""

		self.open_serial_port()
		self.reset()
		self.exec_command('from pyControl import *')
		self.exec_command('import {0} as smd; {0} = sm.State_machine(smd);'.format(task_name))
		for varname, varvalue in variables:
			if varvalue is not None:
				self.exec_command("{0}.smd.v.{1} = ".format(task_name, varname) + varvalue)
