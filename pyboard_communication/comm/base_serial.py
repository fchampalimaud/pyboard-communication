# !/usr/bin/python
# -*- coding: utf-8 -*-

import serial
import logging
import datetime
from pysettings import conf

from pybranch.comm_error import CommError

logger = logging.getLogger(__name__)


class BaseSerial(object):
	def __init__(self, serial_port, baudrate=115200, inter_char_timeout=1):
		"""
		Initialize serial connection settings
		:param str serial_port:
		:param int baudrate:
		:param int inter_char_timeout:
		"""

		self.connection = serial.Serial()

		self.serial_port = serial_port
		self.baudrate = baudrate
		self.inter_char_timeout = inter_char_timeout

	@property
	def connection(self):
		return self._connection  # type: serial.Serial

	@connection.setter
	def connection(self, value):
		self._connection = value  # type: serial.Serial

	@property
	def serial_port(self):
		return self.connection.port

	@serial_port.setter
	def serial_port(self, value):
		self.connection.port = value

	@property
	def baudrate(self):
		return self.connection.baudrate

	@baudrate.setter
	def baudrate(self, value):
		self.connection.baudrate = value

	@property
	def inter_char_timeout(self):
		return self.connection.timeout

	@inter_char_timeout.setter
	def inter_char_timeout(self, value):
		self.connection.timeout = value
